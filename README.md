# ExtendedEditor



## Aim

Create custom editor functionalities in Unreal Engine 5 through different editor modules and slate widgets using C++


- created some asset actions using a menu within editor upon selected assets
    - delete + duplicate
- custom menu entry + fixup redirectors from code 
    - delete empty folder
- Slate widget
    - create custom editor tab
    - send data to slate widget
    - basic layout
    - construct list
    - add checkbox