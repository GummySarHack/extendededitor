// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FSuperManagerModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

private:


	#pragma region ContentBrowserMenuExtension

	void InitCBMenuExtention();

	TSharedRef<FExtender> CustomCBMenuExtender(const TArray<FString>& SelectedPaths);

	void AddCDMenuEntry(class FMenuBuilder& FMenuBuilder);

	void OnDeleteUsuedAssetButtonClicked();
	void OnDeleteUsuedFolderButtonClicked();



	TArray<FString> FolderPathsSelected;

	#pragma endregion


	#pragma region CustomEditorTab

	void OnAdvanceDeletionBtnClicked();
	void RegisterAdvanceDeletionTab();
	TSharedRef<SDockTab> OnSpawnAdvanceDeletionTab(const FSpawnTabArgs& SpawnTabArgs);

	TArray<TSharedPtr <FAssetData>> GetAllAssetDataUnderSelectedFolder();

	#pragma endregion


public:

	#pragma region ProcessDataForDeletionTab

	bool DeleteSingleAsset(const FAssetData& AssetToDelete);
	bool DeleteMultipleAssets(const TArray<FAssetData>& AssetsToDelete);
	void ListUnusedAssets(const TArray<TSharedPtr<FAssetData>>& UnusedAssetsUnfiltered, 
						  TArray<TSharedPtr<FAssetData>>& UnusedAssetsFiltered);
	void ListAssetsSameName(const TArray<TSharedPtr<FAssetData>>& SameNameAssetsUnfiltered,
							TArray<TSharedPtr<FAssetData>>& SameNameAssetsFiltered);
	void SyncCBToClickedAssetForAssetList(const FString& AssetPathToSync);

	#pragma endregion
};
