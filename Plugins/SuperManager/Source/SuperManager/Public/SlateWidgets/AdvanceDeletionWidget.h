// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Widgets/SCompoundWidget.h"

class SAdvanceDeletionTab : public SCompoundWidget
{
	SLATE_BEGIN_ARGS(SAdvanceDeletionTab) {}


	SLATE_ARGUMENT(TArray<TSharedPtr <FAssetData>>, AssetsDataToStore);


	SLATE_END_ARGS();

public:

	void Construct(const FArguments& InArgs);


private:

	#pragma region BuildListView

	TArray<TSharedPtr <FAssetData>> StoredAssetsData;
	TArray<TSharedPtr <FAssetData>> AssetsDataToDelete;
		
	TSharedRef<SListView<TSharedPtr<FAssetData>>> ConstructionAssetList();
	TSharedPtr<SListView<TSharedPtr<FAssetData>>> ConstructedAssetList;

	void RefreshAssetListView();

	void FilterAssetList(const FText& InText);

	//TSharedPtr<FTextFilterExpressionEvaluator> TextFilter;

	TSharedRef<ITableRow> OnGenerateRowForList(TSharedPtr<FAssetData> AssetDataDisplay,
											   const TSharedRef<STableViewBase>& OwnerTable);


	#pragma endregion

	TArray<TSharedRef<SCheckBox>> CheckBoxArray;


	TSharedRef<SCheckBox> ConstructCheckBox(const TSharedPtr<FAssetData> AssetData);
	void OnCheckBoxStateChanged(ECheckBoxState NewState, TSharedPtr<FAssetData> AssetData);

	TSharedRef<STextBlock> ConstructTextForRowWidget(const FString& TextContent, const FSlateFontInfo FontInfo);

	TSharedRef<SButton> ConstructBtnForRowWidget(TSharedPtr<FAssetData> AssetDataDisplay);

	FReply OnDeleteBtnClicked(TSharedPtr<FAssetData> ClickedAssetData);

	void OnRowWidgetMoustButtonClicked(TSharedPtr<FAssetData> ClickedData);
	
	FSlateFontInfo GetEmboseedTextFont() const { return FCoreStyle::Get().GetFontStyle(FName("EmboassedText")); };

	
	
	#pragma region BuildBtnTab

	TSharedRef<SButton> ConstructDeleteAllBtn();
	TSharedRef<SButton> ConstructSelectCheckAllBtn();
	TSharedRef<SButton> ConstructUnSelectCheckAllBtn();

	FReply OnDeleteAllBtnClicked();
	FReply OnSelectAllBtnClicked();
	FReply OnUnSelectAllBtnClicked();

	TSharedRef<STextBlock> BuildTabText(const FString& TextContent);

	#pragma endregion




	#pragma region ComboBox

	TSharedRef<SComboBox<TSharedPtr<FString>>> ConstructComboBox();


	TArray<TSharedPtr<FString>> ComboBoxSourceItems;

	TSharedRef<SWidget> OnGenereteComboContent(TSharedPtr<FString> SourceItem);

	void OnComboSelectionChanged(TSharedPtr<FString> SelectedItem, ESelectInfo::Type InSelectInfo) ;

	TSharedPtr<STextBlock> ComboDisplayText;

	TArray<TSharedPtr <FAssetData>> DisplayedAssetsData;

	#pragma endregion



};