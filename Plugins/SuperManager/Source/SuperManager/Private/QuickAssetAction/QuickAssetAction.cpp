// Fill out your copyright notice in the Description page of Project Settings.


#include "QuickAssetAction/QuickAssetAction.h"
#include "DebugHeader.h"
#include "EditorUtilityLibrary.h"
#include "EditorAssetLibrary.h"
#include "ObjectTools.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "AssetToolsModule.h"

using namespace DebugHeader;

void UQuickAssetAction::DuplicateAssets(int32 NumDuplicates)
{
	if (NumDuplicates <= 0)
	{
		ShowMsgDialog(EAppMsgType::Ok,TEXT("Please enter a VALID number"), true);

		//Print(TEXT("Please enter a VALID number"), FColor::Red);
		return;
	}

	TArray<FAssetData> SelectedAssetsData = UEditorUtilityLibrary::GetSelectedAssetData();

	uint32 counter = 0;

	for (const FAssetData& selectedAssetData : SelectedAssetsData)
	{
		for (int32 i = 0; i < NumDuplicates; i++)
		{
			const FString sourceAssetPath = selectedAssetData.ObjectPath.ToString();
			const FString newDuplicated = selectedAssetData.AssetName.ToString() + TEXT("_") + FString::FromInt(i+1);
			const FString newPathName = FPaths::Combine(selectedAssetData.PackagePath.ToString(), newDuplicated);
		
			if (UEditorAssetLibrary::DuplicateAsset(sourceAssetPath, newPathName))
			{
				UEditorAssetLibrary::SaveAsset(newPathName, false);
				++counter;
			}
		}
	}

	if (counter > 0)
	{
		ShowNotifyInfo(TEXT("Successfully duplicated " + FString::FromInt(counter) + " assets"));
	}

	//Print(TEXT("Assets were duplicated " + FString::FromInt(counter)), FColor::Green);
}

void UQuickAssetAction::AddPrefixed()
{
	TArray<UObject*> SelectedObjects = UEditorUtilityLibrary::GetSelectedAssets();

	uint32 counter = 0;

	for (UObject* selectedObject : SelectedObjects)
	{
		if (!selectedObject)
		{
			continue;
		}

		FString* prefixFound = PrefixMap.Find(selectedObject->GetClass());

		if (!prefixFound || prefixFound->IsEmpty())
		{
			Print(TEXT("Failed to find prefix for class ") + selectedObject->GetClass()->GetName(), FColor::Red);
			continue;
		}

		FString oldName = selectedObject->GetName();

		if (oldName.StartsWith(*prefixFound))
		{
			Print(oldName + TEXT(" already has correct prefix "), FColor::Red);
			continue;
		}

		
		if (selectedObject->IsA<UMaterialInstanceConstant>())
		{
			oldName.RemoveFromStart(TEXT("M_"));
			oldName.RemoveFromEnd(TEXT("_Inst"));
		}


		//add prefix

		const FString newNameWithPrefix = *prefixFound + oldName;
		UEditorUtilityLibrary::RenameAsset(selectedObject, newNameWithPrefix);

		++counter;
	}

	if (counter > 0)
	{
		ShowNotifyInfo(TEXT("Successfully rename " + FString::FromInt(counter) + " assets"));
	}
}

void UQuickAssetAction::AddStaticMesh_Box()
{
	ShowNotifyInfo(TEXT("ADD STATIC BOX"));
}

void UQuickAssetAction::RemoteUsuedAssets()
{
	TArray<FAssetData> SelectedAssetsData = UEditorUtilityLibrary::GetSelectedAssetData();
	TArray<FAssetData> UsuedAssetData = UEditorUtilityLibrary::GetSelectedAssetData();

	FixRedirectors();

	for (const FAssetData& selectedAsset : SelectedAssetsData)
	{
		TArray<FString> assetReferencers =
			UEditorAssetLibrary::FindPackageReferencersForAsset(selectedAsset.ObjectPath.ToString());

		if (assetReferencers.Num() == 0)
		{
			UsuedAssetData.Add(selectedAsset);
		}
	}

	if (UsuedAssetData.Num() == 0)
	{
		ShowMsgDialog(EAppMsgType::Ok,TEXT("No usued asset found"), false);
		return;
	}
	const int32 numAssetsDeleted = ObjectTools::DeleteAssets(UsuedAssetData);

	if (numAssetsDeleted == 0)
	{
		return;
	}
	
	ShowNotifyInfo(TEXT("Successfully deleted " + FString::FromInt(numAssetsDeleted)) + TEXT(" usued assets"));
}

void UQuickAssetAction::Test()
{
	TArray<FAssetData> SelectedAssetsData = UEditorUtilityLibrary::GetSelectedAssetData();

	for (const FAssetData& selectedAsset : SelectedAssetsData)
	{
		ShowNotifyInfo(TEXT("Successfully rename " + selectedAsset.AssetName.ToString() + " assets"));
	}

}

void UQuickAssetAction::FixRedirectors()
{
	TArray<UObjectRedirector*> RedirectorToFix;

	FAssetRegistryModule& AssetRegistryModule = 
		FModuleManager::Get().LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));

	FARFilter filter;

	filter.bRecursivePaths = true;
	filter.PackagePaths.Emplace("/Game");
	filter.ClassNames.Emplace("ObjectRedirector");

	TArray<FAssetData> OutRedirectors;

	AssetRegistryModule.Get().GetAssets(filter, OutRedirectors);

	for (const FAssetData& assetData : OutRedirectors)
	{
		if (UObjectRedirector* redirectiorToFix = Cast<UObjectRedirector>(assetData.GetAsset()))
		{

			RedirectorToFix.Add(redirectiorToFix);
		}
	}

	FAssetToolsModule& assetModule = FModuleManager::LoadModuleChecked<FAssetToolsModule>(TEXT("AssetTools"));

	assetModule.Get().FixupReferencers(RedirectorToFix);
}
