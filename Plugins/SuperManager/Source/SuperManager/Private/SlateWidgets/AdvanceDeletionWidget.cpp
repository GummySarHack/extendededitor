

// Fill out your copyright notice in the Description page of Project Settings.


#include "SlateWidgets/AdvanceDeletionWidget.h"
#include "SlateBasics.h"
#include "DebugHeader.h"
#include "SuperManager.h"
#include "Widgets/Input/SSearchBox.h"

using namespace DebugHeader;

#define ListAll TEXT("List All Assets")
#define ListUsued TEXT("List All Unused Assets")
#define ListSameName TEXT("List All Assets With Same Name")

void SAdvanceDeletionTab::Construct(const FArguments& InArgs)
{
	bCanSupportFocus = true;

	//receives value that gets passed in arg
	//InArgs.AssetsDataUnderSelectedFolderArray;

	StoredAssetsData = InArgs._AssetsDataToStore;
	DisplayedAssetsData = StoredAssetsData;
	ComboBoxSourceItems.Empty();

	ComboBoxSourceItems.Add(MakeShared<FString>(ListAll));
	ComboBoxSourceItems.Add(MakeShared<FString>(ListUsued));
	ComboBoxSourceItems.Add(MakeShared<FString>(ListSameName));


	auto font = GetEmboseedTextFont();
	font.Size = 30;
	auto font2 = GetEmboseedTextFont();
	font2.Size = 15;

	//assembles the datas 
	ChildSlot
	[
		//main vertical box
		SNew(SVerticalBox)

		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(STextBlock)
			.Text(FText::FromString(TEXT("Advance deletion")))
			.Font(font)
			.Justification(ETextJustify::Center)
			.ColorAndOpacity(FColor::White)
		]

		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SSearchBox)
			.HintText(FText::FromString(TEXT("Search")))
			.OnTextChanged(this, &SAdvanceDeletionTab::FilterAssetList)
		]


		//Dropdown specifier
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)

			//this combostuff
			+SHorizontalBox::Slot()
			.AutoWidth()
			[
				ConstructComboBox()
			]

			//help text
			+ SHorizontalBox::Slot()
			.FillWidth(0.1f)
			[
				SNew(STextBlock)
				.Text(FText::FromString(TEXT("Specificy listing condition for the combo box on the right")))
				.Font(font2)
				.Justification(ETextJustify::Center)
				.ColorAndOpacity(FColor::White)
			]
		]


		//Actual dropdown
		+SVerticalBox::Slot().VAlign(VAlign_Fill)
		[
			SNew(SScrollBox)

			+ SScrollBox::Slot()
			[
				ConstructionAssetList()
			]
		]

		//btns slot
		+ SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)

			//first btn
			+SHorizontalBox::Slot()
			.FillWidth(10.f)
			.Padding(5.f)
			[
				ConstructDeleteAllBtn()
			]

			//first btn
			+ SHorizontalBox::Slot()
			.FillWidth(10.f)
			.Padding(5.f)
			[
				ConstructSelectCheckAllBtn()
			]
			
			+ SHorizontalBox::Slot()
			.FillWidth(10.f)
			.Padding(5.f)
			[
				ConstructUnSelectCheckAllBtn()
			]

		]
	];
}

#pragma region BuildListView

TSharedRef<SListView<TSharedPtr<FAssetData>>> SAdvanceDeletionTab::ConstructionAssetList()
{
	ConstructedAssetList =
		SNew(SListView<TSharedPtr<FAssetData>>)
		.ItemHeight(24.f)
		.ListItemsSource(&DisplayedAssetsData)
		.OnGenerateRow(this, &SAdvanceDeletionTab::OnGenerateRowForList)
		.OnMouseButtonClick(this, &SAdvanceDeletionTab::OnRowWidgetMoustButtonClicked);

	return ConstructedAssetList.ToSharedRef();
}

void SAdvanceDeletionTab::RefreshAssetListView()
{
	CheckBoxArray.Empty();
	AssetsDataToDelete.Empty();
	
	if (ConstructedAssetList.IsValid())
	{
		ConstructedAssetList->RebuildList();
	}
}

TSharedRef<ITableRow> SAdvanceDeletionTab::OnGenerateRowForList(
	TSharedPtr<FAssetData> AssetDataDisplay,
	const TSharedRef<STableViewBase>& OwnerTable)
{

	if (!AssetDataDisplay.IsValid())
	{
		return SNew(STableRow<TSharedPtr<FAssetData>>, OwnerTable);
	}

	const FString DisplayName = AssetDataDisplay->AssetName.ToString();
	const FString DisplayNamePath = AssetDataDisplay->ObjectPath.ToString();
	const FString DisplayAssetClassName = AssetDataDisplay->AssetClass.ToString();
	FSlateFontInfo AssetClassNameFont = GetEmboseedTextFont();
	AssetClassNameFont.Size = 10;
	FSlateFontInfo AssetNameFont = GetEmboseedTextFont();
	AssetClassNameFont.Size = 15;

	TSharedRef<STableRow<TSharedPtr <FAssetData>>> ListViewRow =
		SNew(STableRow<TSharedPtr<FAssetData>>, OwnerTable)
		.Padding(FMargin(0.5f))
		[

			SNew(SHorizontalBox)

			//first slot - check box
			+SHorizontalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Center)
			.FillWidth(.05f)
			[
				ConstructCheckBox(AssetDataDisplay)
			]


			//second slot - display asset class name
			+SHorizontalBox::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Fill)
			.FillWidth(.2f)
			[
				ConstructTextForRowWidget(DisplayAssetClassName, AssetClassNameFont)
			]


			//thrid slot - actual name
			+SHorizontalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Fill)
			.FillWidth(.5f)
			[
				ConstructTextForRowWidget(DisplayName, AssetClassNameFont)
			]

			//path 
			+ SHorizontalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Fill)
			.FillWidth(.3f)
			[
				ConstructTextForRowWidget(DisplayNamePath, AssetClassNameFont)
			]


			//fourth - btn 
			+SHorizontalBox::Slot()
			.HAlign(HAlign_Right)
			.VAlign(VAlign_Fill)
			.FillWidth(.2f)
			[
				ConstructBtnForRowWidget(AssetDataDisplay)
			]

		];

	return ListViewRow;
}

TSharedRef<STextBlock> SAdvanceDeletionTab::ConstructTextForRowWidget(const FString& TextContent, const FSlateFontInfo FontInfo)
{
	TSharedRef<STextBlock> ConstructTextBlock =
		SNew(STextBlock)
		.Text(FText::FromString(TextContent))
		.Font(FontInfo)
		.ColorAndOpacity(FColor::White);

	return ConstructTextBlock;
}

#pragma endregion

TSharedRef<SButton> SAdvanceDeletionTab::ConstructBtnForRowWidget(TSharedPtr<FAssetData> AssetDataDisplay)
{
	TSharedRef<SButton> ConstructDeleteBtn =
		SNew(SButton)
		.Text(FText::FromString("Delete"))
		.OnClicked(this, &SAdvanceDeletionTab::OnDeleteBtnClicked, AssetDataDisplay);

	return ConstructDeleteBtn;
}

void SAdvanceDeletionTab::FilterAssetList(const FText& InText)
{
	//temporary list
	TArray<TSharedPtr <FAssetData>> AssetsDataList_Temp;

	if (!InText.ToString().IsEmpty())
	{
		for (int assetIndex = 0; assetIndex< StoredAssetsData.Num(); assetIndex++)
		{
			auto asset = StoredAssetsData[assetIndex];

			if (asset.Get()->AssetName.ToString().Contains(InText.ToString()))
			{
				AssetsDataList_Temp.Add(asset);
			}
		}
		DisplayedAssetsData = AssetsDataList_Temp;
	}
	else
	{
		DisplayedAssetsData = StoredAssetsData;
	}
}

FReply SAdvanceDeletionTab::OnDeleteBtnClicked(TSharedPtr<FAssetData> AssetData)
{
	PrintLog(TEXT("THIS: ") + AssetData->AssetName.ToString());
	
	FSuperManagerModule SuperModule = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));


	const bool bAssetDeleted = SuperModule.DeleteSingleAsset(*AssetData.Get());

	if (bAssetDeleted)
	{
		if (StoredAssetsData.Contains(AssetData))
		{
			StoredAssetsData.Remove(AssetData);
		}

		StoredAssetsData.Remove(AssetData);

		if (DisplayedAssetsData.Contains(AssetData))
		{
			DisplayedAssetsData.Remove(AssetData);
		}
	}


	return FReply::Handled();
}


#pragma region BuildBtnTab

TSharedRef<SButton> SAdvanceDeletionTab::ConstructDeleteAllBtn()
{
	TSharedRef<SButton> DelAllBtn =
		SNew(SButton)
		.ContentPadding(FMargin(5.f))
		.OnClicked(this, &SAdvanceDeletionTab::OnDeleteAllBtnClicked);

	DelAllBtn->SetContent(BuildTabText(TEXT("Delete All")));

	return DelAllBtn;
}

TSharedRef<SButton> SAdvanceDeletionTab::ConstructSelectCheckAllBtn()
{
	TSharedRef<SButton> ToggleAllBtn =
		SNew(SButton)
		.ContentPadding(FMargin(5.f))
		.OnClicked(this, &SAdvanceDeletionTab::OnSelectAllBtnClicked);

	ToggleAllBtn->SetContent(BuildTabText(TEXT("Select All")));

	return ToggleAllBtn;
}

TSharedRef<SButton> SAdvanceDeletionTab::ConstructUnSelectCheckAllBtn()
{
	TSharedRef<SButton> ToggleAllBtn =
		SNew(SButton)
		.ContentPadding(FMargin(5.f))
		.OnClicked(this, &SAdvanceDeletionTab::OnUnSelectAllBtnClicked);

	ToggleAllBtn->SetContent(BuildTabText(TEXT("Unselect All")));

	return ToggleAllBtn;
}

void SAdvanceDeletionTab::OnRowWidgetMoustButtonClicked(TSharedPtr<FAssetData> ClickedData)
{
	FSuperManagerModule& SuperManagerModule =
		FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));

	SuperManagerModule.SyncCBToClickedAssetForAssetList(ClickedData->ObjectPath.ToString());
}

FReply SAdvanceDeletionTab::OnDeleteAllBtnClicked()
{
	PrintLog(TEXT("DELETE"));

	FSuperManagerModule SuperModule = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));

	return FReply::Handled();
}


FReply SAdvanceDeletionTab::OnSelectAllBtnClicked()
{
	PrintLog(TEXT("SELECT"));

	if (CheckBoxArray.Num() >= 0)
	{
		for (const TSharedRef<SCheckBox> checkBox : CheckBoxArray)
		{
			if (checkBox->GetCheckedState() != ECheckBoxState::Checked)
				checkBox->ToggleCheckedState();
		}
	}

	return FReply::Handled();
}

FReply SAdvanceDeletionTab::OnUnSelectAllBtnClicked()
{
	PrintLog(TEXT("UNSELECT"));

	if (CheckBoxArray.Num() >= 0)
	{
		for (const TSharedRef<SCheckBox> checkBox : CheckBoxArray)
		{
			if (checkBox->GetCheckedState() != ECheckBoxState::Unchecked)
				checkBox->ToggleCheckedState();
		}
	}

	return FReply::Handled();
}

TSharedRef<STextBlock> SAdvanceDeletionTab::BuildTabText(const FString& TextContent)
{
	FSlateFontInfo BntTextFont = GetEmboseedTextFont();
	BntTextFont.Size = 15;

	TSharedRef<STextBlock> TextBlock =
		SNew(STextBlock)
		.Text(FText::FromString(TextContent))
		.Font(BntTextFont)
		.Justification(ETextJustify::Center);

	return TextBlock;
}

#pragma endregion


#pragma region ComboBox


TSharedRef<SComboBox<TSharedPtr<FString>>> SAdvanceDeletionTab::ConstructComboBox()
{
	TSharedRef<SComboBox<TSharedPtr<FString>>> buildComboBox =
		SNew(SComboBox<TSharedPtr<FString>>)
		.OptionsSource(&ComboBoxSourceItems)
		.OnGenerateWidget(this, &SAdvanceDeletionTab::OnGenereteComboContent)
		.OnSelectionChanged(this, &SAdvanceDeletionTab::OnComboSelectionChanged)
		[
			SAssignNew(ComboDisplayText, STextBlock)
			.Text(FText::FromString(TEXT("List Asets Option")))
		];

	return buildComboBox;
}

TSharedRef<SWidget> SAdvanceDeletionTab::OnGenereteComboContent(TSharedPtr<FString> SourceItem)
{
	TSharedRef<STextBlock> buildComboText =
		SNew(STextBlock)
		.Text(FText::FromString(*SourceItem.Get()));

	return buildComboText;
}

void SAdvanceDeletionTab::OnComboSelectionChanged(TSharedPtr<FString> SelectedItem, ESelectInfo::Type InSelectInfo)
{
	PrintLog(*SelectedItem.Get());

	ComboDisplayText->SetText(FText::FromString(*SelectedItem.Get()));

	//pass data to module to filter

	FSuperManagerModule SuperModule = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));

	if (*SelectedItem.Get() == ListAll)
	{
		// list all
		DisplayedAssetsData = StoredAssetsData;
	}
	else if (*SelectedItem.Get() == ListUsued)
	{
		//list all unused data
		SuperModule.ListUnusedAssets(StoredAssetsData, DisplayedAssetsData);
	}
	else if (*SelectedItem.Get() == ListSameName)
	{
		//list all unused data
		SuperModule.ListAssetsSameName(StoredAssetsData, DisplayedAssetsData);
	}

	RefreshAssetListView();
}



#pragma endregion


TSharedRef<SCheckBox> SAdvanceDeletionTab::ConstructCheckBox(const TSharedPtr<FAssetData> AssetData)
{
	TSharedRef<SCheckBox> ConstructedCheckedBox =
		SNew(SCheckBox)
			.Type(ESlateCheckBoxType::CheckBox)
			.OnCheckStateChanged(this, &SAdvanceDeletionTab::OnCheckBoxStateChanged, AssetData)
			.Visibility(EVisibility::Visible);

	CheckBoxArray.Add(ConstructedCheckedBox);

	return ConstructedCheckedBox;
}

void SAdvanceDeletionTab::OnCheckBoxStateChanged(ECheckBoxState NewState, TSharedPtr<FAssetData> AssetData)
{
	switch (NewState)
	{
		case ECheckBoxState::Unchecked:
			Print("unchecked: " + AssetData->AssetName.ToString(), FColor::Magenta);
			break;
		case ECheckBoxState::Checked:
			Print("checked: " + AssetData->AssetName.ToString(), FColor::Green);
			break;
		case ECheckBoxState::Undetermined:
			break;
		default:
			break;
	}
}