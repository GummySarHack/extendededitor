// Copyright Epic Games, Inc. All Rights Reserved.

#include "SuperManager.h"

#include "ContentBrowserModule.h"
#include "DebugHeader.h"
#include "EditorAssetLibrary.h"
#include "ObjectTools.h"
#include "SlateWidgets/AdvanceDeletionWidget.h"
#include "SuperManager/CustomStyle/SuperManagerStyle.h"


#define LOCTEXT_NAMESPACE "FSuperManagerModule"

using namespace DebugHeader;

void FSuperManagerModule::StartupModule()
{
	FSuperManagerStyle::InitializeIcons();

	//create a module
	InitCBMenuExtention();

	RegisterAdvanceDeletionTab();
}

void FSuperManagerModule::ShutdownModule()
{
	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(FName("AdvanceDeletion"));
	FSuperManagerStyle::ShutDown();
}


#pragma region ContentBrowserMenuExtension

void FSuperManagerModule::InitCBMenuExtention()
{
	FContentBrowserModule& ContentBrowserModule = 
		FModuleManager::LoadModuleChecked<FContentBrowserModule>(TEXT("ContentBrowser"));

	TArray<FContentBrowserMenuExtender_SelectedPaths>& contentBrowserModuleExt = 
		ContentBrowserModule.GetAllPathViewContextMenuExtenders();

	contentBrowserModuleExt.Add
	(
		FContentBrowserMenuExtender_SelectedPaths::CreateRaw(this, &FSuperManagerModule::CustomCBMenuExtender)
	);
}

TSharedRef<FExtender> FSuperManagerModule::CustomCBMenuExtender(const TArray<FString>& SelectedPaths)
{
	TSharedRef<FExtender> menuExtender(new FExtender());

	if (SelectedPaths.Num() > 0)
	{
		menuExtender->AddMenuExtension
		(
			FName("Delete"),
			EExtensionHook::After,
			TSharedPtr<FUICommandList>(),
			FMenuExtensionDelegate::CreateRaw(this, &FSuperManagerModule::AddCDMenuEntry)
		);

		FolderPathsSelected = SelectedPaths;
	}

	return menuExtender;
}

void FSuperManagerModule::AddCDMenuEntry(FMenuBuilder& FMenuBuilder)
{
	FMenuBuilder.AddMenuEntry
	(
		FText::FromString(TEXT("Delete Usued Assets")),
		FText::FromString(TEXT("Safely delete all usued asset under folder")),
		FSlateIcon(FSuperManagerStyle::GetStyleSetName(),"ContentBrowser.DeleteUnusuedAssets" ),
		FExecuteAction::CreateRaw(this, &FSuperManagerModule::OnDeleteUsuedAssetButtonClicked)
	);

	FMenuBuilder.AddMenuEntry
	(
		FText::FromString(TEXT("Delete Usued Folders")),
		FText::FromString(TEXT("Safely delete all usued folder")),
		FSlateIcon(FSuperManagerStyle::GetStyleSetName(), "ContentBrowser.DeleteEmptyFolders"),
		FExecuteAction::CreateRaw(this, &FSuperManagerModule::OnDeleteUsuedFolderButtonClicked)
	);

	FMenuBuilder.AddMenuEntry
	(
		FText::FromString(TEXT("Advance deletion")),
		FText::FromString(TEXT("list assets by specific condition in a tab")),
		FSlateIcon(FSuperManagerStyle::GetStyleSetName(), "ContentBrowser.AdvanceDeletion"),
		FExecuteAction::CreateRaw(this, &FSuperManagerModule::OnAdvanceDeletionBtnClicked)
	);

}

void FSuperManagerModule::OnDeleteUsuedAssetButtonClicked()
{
	PrintLog(TEXT("DELETE"));


	if (FolderPathsSelected.Num() > 1)
	{
		ShowMsgDialog(EAppMsgType::Ok,TEXT("You can only do this to one folder"));
		return;
	}

	TArray<FString> AssetsPathNames = UEditorAssetLibrary::ListAssets(FolderPathsSelected[0]);


	if (AssetsPathNames.Num() == 0)
	{
		ShowMsgDialog(EAppMsgType::Ok, TEXT("No assets found under that folder"));
		return;
	}

	EAppReturnType::Type Confirm = ShowMsgDialog(EAppMsgType::YesNo, TEXT("A total of " + FString::FromInt(AssetsPathNames.Num()) + 
										   TEXT(" found. /n Would you like to proceed?")));

	if (Confirm == EAppReturnType::No)
	{
		return;
	}

	TArray<FAssetData> UsuedAssetsData;


	for (const FString& assetPathName : AssetsPathNames)
	{
		//don't touch root folder
		if (assetPathName.Contains(TEXT("Developers")) ||
			assetPathName.Contains(TEXT("Collections")) ||
			assetPathName.Contains(TEXT("__ExternalActors__")) ||
			assetPathName.Contains(TEXT("__ExternalObjects__")))
		{
			continue;
		}

		if (!UEditorAssetLibrary::DoesAssetExist(assetPathName))
		{
			continue;
		}

		TArray<FString> AssetReferences = UEditorAssetLibrary::FindPackageReferencersForAsset(assetPathName);

		if (AssetReferences.Num() == 0)
		{
			const FAssetData usuedAssetData = UEditorAssetLibrary::FindAssetData(assetPathName);
			UsuedAssetsData.Add(usuedAssetData);
		}
	}

	if (UsuedAssetsData.Num() > 0)
	{
		ObjectTools::DeleteAssets(UsuedAssetsData);
	}
	else
	{
		ShowMsgDialog(EAppMsgType::Ok, TEXT("No assets found under selected folder"));
	}

}

void FSuperManagerModule::OnDeleteUsuedFolderButtonClicked()
{
	//not done - FixUpRedirectors();

	TArray<FString> FolderPathsArray = UEditorAssetLibrary::ListAssets(FolderPathsSelected[0], true, true);
	uint32 Counter = 0;

	FString EmptyFolderPathsNames;
	TArray<FString> EmptyFoldersPathsArray;

	for (const FString& FolderPath : FolderPathsArray)
	{
		if (FolderPath.Contains(TEXT("Developers")) ||
			FolderPath.Contains(TEXT("Collections")) ||
			FolderPath.Contains(TEXT("__ExternalActors__")) ||
			FolderPath.Contains(TEXT("__ExternalObjects__")))
		{
			continue;
		}

		if (!UEditorAssetLibrary::DoesDirectoryExist(FolderPath)) continue;

		if (!UEditorAssetLibrary::DoesDirectoryHaveAssets(FolderPath))
		{
			EmptyFolderPathsNames.Append(FolderPath);
			EmptyFolderPathsNames.Append(TEXT("\n"));

			EmptyFoldersPathsArray.Add(FolderPath);
		}
	}

	if (EmptyFoldersPathsArray.Num() == 0)
	{
		DebugHeader::ShowMsgDialog(EAppMsgType::Ok, TEXT("No empty folder found under selected folder"), false);
		return;
	}

	EAppReturnType::Type ConfirmResult = DebugHeader::ShowMsgDialog(EAppMsgType::OkCancel,
																	TEXT("Empty folders found in:\n") + EmptyFolderPathsNames + TEXT("\nWould you like to delete all?"), false);

	if (ConfirmResult == EAppReturnType::Cancel) return;

	for (const FString& EmptyFolderPath : EmptyFoldersPathsArray)
	{
		UEditorAssetLibrary::DeleteDirectory(EmptyFolderPath) ?
			++Counter : DebugHeader::Print(TEXT("Failed to delete " + EmptyFolderPath), FColor::Red);
	}

	if (Counter > 0)
	{
		DebugHeader::ShowNotifyInfo(TEXT("Successfully deleted ") + FString::FromInt(Counter) + TEXT("folders"));
	}
}


#pragma endregion


#pragma region CustomEditorTab


void FSuperManagerModule::OnAdvanceDeletionBtnClicked()
{
	PrintLog(TEXT("AdvanceDeletion"));

	FGlobalTabmanager::Get()->TryInvokeTab(FName("AdvanceDeletion"));
}

//Create a tab
void FSuperManagerModule::RegisterAdvanceDeletionTab()
{
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(
		FName("AdvanceDeletion"), 
		FOnSpawnTab::CreateRaw(this, &FSuperManagerModule::OnSpawnAdvanceDeletionTab)).
		SetDisplayName(FText::FromString(TEXT("Advance Deletion"))).
		SetIcon(FSlateIcon(FSuperManagerStyle::GetStyleSetName(), "ContentBrowser.AdvanceDeletion"));
}

//Spawns tab
TSharedRef<SDockTab> FSuperManagerModule::OnSpawnAdvanceDeletionTab(const FSpawnTabArgs& SpawnTabArgs)
{
	//sends data to widget
	return
		SNew(SDockTab).TabRole(ETabRole::NomadTab)
		[
			SNew(SAdvanceDeletionTab).AssetsDataToStore(GetAllAssetDataUnderSelectedFolder())
		];
}

TArray<TSharedPtr<FAssetData>> FSuperManagerModule::GetAllAssetDataUnderSelectedFolder()
{


	 TArray<TSharedPtr<FAssetData>> AvailableAssetsData;

	 if (FolderPathsSelected.IsEmpty()) return TArray<TSharedPtr<FAssetData>>();

	 TArray<FString> AssetsPathNames = UEditorAssetLibrary::ListAssets(FolderPathsSelected[0]);

	 for (const FString& assetPathName : AssetsPathNames)
	 {
		 //don't touch root folder
		 if (assetPathName.Contains(TEXT("Developers")) ||
			 assetPathName.Contains(TEXT("Collections")) ||
			 assetPathName.Contains(TEXT("__ExternalActors__")) ||
			 assetPathName.Contains(TEXT("__ExternalObjects__")))
		 {
			 continue;
		 }

		 if (!UEditorAssetLibrary::DoesAssetExist(assetPathName))
		 {
			 continue;
		 }


		 const FAssetData Data = UEditorAssetLibrary::FindAssetData(assetPathName);

		 AvailableAssetsData.Add(MakeShared<FAssetData>(Data));
	 }

	 return AvailableAssetsData;
}

#pragma endregion


#pragma region ProcessDataForDeletionTab

bool FSuperManagerModule::DeleteSingleAsset(const FAssetData& AssetToDelete)
{
	TArray<FAssetData> ArrayDataToDelete;

	ArrayDataToDelete.Add(AssetToDelete);

	return ObjectTools::DeleteAssets(ArrayDataToDelete) > 0;
}

bool FSuperManagerModule::DeleteMultipleAssets(const TArray<FAssetData>& AssetsToDelete)
{
	return ObjectTools::DeleteAssets(AssetsToDelete) > 0;
}

void FSuperManagerModule::ListUnusedAssets(const TArray<TSharedPtr<FAssetData>>& UnusedAssetsUnfiltered, 
										   TArray<TSharedPtr<FAssetData>>& UnusedAssetsFiltered)
{
	UnusedAssetsFiltered.Empty();

	for (const TSharedPtr<FAssetData> dataSharedPtr : UnusedAssetsUnfiltered)
	{
		TArray<FString> assetRef =
			UEditorAssetLibrary::FindPackageReferencersForAsset(dataSharedPtr->ObjectPath.ToString());

		if (assetRef.Num() == 0)
		{
			UnusedAssetsFiltered.Add(dataSharedPtr);
		}
	}
}

void FSuperManagerModule::ListAssetsSameName(const TArray<TSharedPtr<FAssetData>>& SameNameAssetsUnfiltered, 
											 TArray<TSharedPtr<FAssetData>>& SameNameAssetsFiltered)
{
	SameNameAssetsFiltered.Empty();

	//finding assets
	TMultiMap<FString, TSharedPtr<FAssetData>> AssetsInfoMap;

	for (const TSharedPtr<FAssetData> dataSharedPtr : SameNameAssetsUnfiltered)
	{
		AssetsInfoMap.Emplace(dataSharedPtr->AssetName.ToString(), dataSharedPtr);
	}


	for (const TSharedPtr<FAssetData> dataSharedPtr : SameNameAssetsUnfiltered)
	{
		TArray<TSharedPtr<FAssetData>> outAssetsData;

		AssetsInfoMap.MultiFind(dataSharedPtr->AssetName.ToString(), outAssetsData);

		if (outAssetsData.Num() <= 1) continue;
		
		for (const TSharedPtr<FAssetData>& sameNameData : outAssetsData)
		{
			if (sameNameData.IsValid())
			{
				SameNameAssetsFiltered.AddUnique(sameNameData);
			}
		}
	}
}

void FSuperManagerModule::SyncCBToClickedAssetForAssetList(const FString& AssetPathToSync)
{
	TArray<FString> AssetsPathToSync;
	AssetsPathToSync.Add(AssetPathToSync);

	UEditorAssetLibrary::SyncBrowserToObjects(AssetsPathToSync);
}

#pragma endregion







#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FSuperManagerModule, SuperManager)