// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "NormalDistributionFactory.generated.h"

/**
 * What allows us to show the new asset-type in the Editor
 */
UCLASS()
class ASSETTUTORIALPLUGINEDITOR_API UNormalDistributionFactory : public UFactory
{
	GENERATED_BODY()

public:
	
	UNormalDistributionFactory();
	UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn);
};